function LoginForm () {
    const history = ReactRouterDOM.useHistory();
    const [email, setEmail] = React.useState('');
    const [password, setPass] = React.useState('');

    const handleChange = (event) => {
        if (event.target.id === 'email'){
            setEmail(event.target.value);
        } else {
            setPass(event.target.value);
        }
    }

    const doLogin = (event) => {
        event.preventDefault();
        axios.post('/api/v1/login', {
            email: email,
            password: password
        })
        .then((response) => {
            sessionStorage['user'] = JSON.stringify(response);
            if(response.data.type === 'user') {
                history.push('/home')
            } else {
                history.push('/admin')
            }
        })
        .catch(function (error) {
            console.error(error);
        });
    }

    return (
    <form onSubmit={doLogin}>
        <fieldset>
            <label htmlFor="email">Email</label>
            <input type="email" placeholder="email@example.com" id="email" onChange={handleChange}/>
            <label htmlFor="email">Password</label>
            <input type="password" placeholder="*****" id="password"  onChange={handleChange} />
            <button className="button-primary">Login</button>
        </fieldset>
    </form>
    );
}