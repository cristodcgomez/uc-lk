class Admin extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {
                id: 0,
                name: ''
            },
            products: {
                head: [],
                body: []
            },
            discounts: {
                head: [],
                body: []
            },
            orders: {
                head: [],
                body: []
            },
            view: 'orders'
        };
    }

    generateDataTable(input) {
        return {
            head: Object.keys(input[0]),
            body: input.map(el => Object.values(el))
        }
    }

    componentDidMount() {
        this._isMounted = true;
        let user = JSON.parse(sessionStorage['user']).data;
        let admin = this.state.user;
        admin.id = user.id;
        admin.name = user.name;
        
        let getProducts = axios.get('/api/v1/product');
        let getOrders = axios.get('/api/v1/order');
        let getDiscounts = axios.get('/api/v1/discount');
        axios.all([getProducts, getOrders, getDiscounts])
        .then(axios.spread((products, orders, discounts) => {
            this.setState({
                user: admin,
                products: this.generateDataTable(products.data),
                orders: this.generateDataTable(orders.data),
                discounts: this.generateDataTable(discounts.data)
            });
        }));
    }

    setView(value) {
        this.setState({
            view: value
        });
    }
    
    render() {
        let viewBlock;

        switch (this.state.view) {
            case 'orders':
                viewBlock =  <Table title={'Orders'} uri={'order'} data={this.state.orders} filter={'status'}></Table>
                break;
            case 'products':
                viewBlock =  <Table title={'Products'} uri={'product'} data={this.state.products} enableCreate={true}></Table>
                break;
            case 'discounts':
                viewBlock =  <Table title={'Discounts'} uri={'discount'} data={this.state.discounts} enableCreate={true}></Table>
                break;
            default:
                break;
        }
        
        return (
        <React.Fragment>
                <Header title="Admin" user={this.state.user}></Header>
                <div className="row">
                    <aside className="column column-25 side-menu">
                        <h2 className="header">Menu</h2>
                        <nav className="row">
                            <div className="column">
                                <button className="button  button-clear btn-block" onClick={ () => this.setView('orders')}>Orders</button>
                                <button className="button  button-clear btn-block" onClick={ () => this.setView('products')}>Products</button>
                                <button className="button  button-clear btn-block" onClick={ () => this.setView('discounts')}>Discounts</button>
                            </div>
                        </nav>
                    </aside>
                    <section className="column column-75">
                        <div className="card">
                            <div className="card-body">
                                {viewBlock}
                            </div>
                        </div>
                    </section>
                </div>
            </React.Fragment>
        );
    }
}