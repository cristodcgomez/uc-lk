class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {
                id: 0,
                name: ''
            },
            productList: [],
            order: {
                number: 0,
                date: new Date(),
                user: 0,
                status: 'draft',
                products: [],
                discount_applied: '',
            }
        };
        this.add = this.add.bind(this);
        this.removeProduct = this.removeProduct.bind(this);
    }

    componentDidMount() {
        let user = JSON.parse(sessionStorage['user']).data;
        let userOrder = this.state.user;
        userOrder.id = user.id;
        userOrder.name = user.name;

        let order = this.state.order;
        order.user = user.id;

        this.setState({user: userOrder});

        axios.get('/api/v1/product')
        .then((response) => {
            this.setState({productList: response.data});
        })
        .catch(function (error) {
            console.error(error);
        });
    }

    add(value) {
        let newItem = this.state.order;
        newItem.products.push(value);
        this.setState({
            order: newItem,
        });
    }

    removeProduct(value) {
        let ind = this.state.order.products.findIndex(el => el.id === value.id);
        if(ind !== -1) {
            let order = this.state.order;
            order.products.splice(ind,1);
            this.setState({
                order: order,
            });
        }
        let productInd = this.state.productList.findIndex(el => el.id === value.id);
        if (productInd !== -1) {
            let productList = this.state.productList;
            productList[productInd].selected = false;
            this.setState({
                productList: productList,
            });
        }
    }

    render() {
        return (
            <React.Fragment>
                <Header title="Home" user={this.state.user}></Header>
                <div className="row">
                    <section className="column column-50 column-offset-10">
                        <h2 className="header">Products available</h2>
                        <ProductList products={this.state.productList} onAdd={this.add}></ProductList>
                    </section>
                    <aside className="column column-30 column-offset-10 side-menu">
                        <h2 className="header">My order</h2>
                        <Order order={this.state.order} onRemove={this.removeProduct}></Order>
                    </aside>
                </div>
            </React.Fragment>
        );
    }
}