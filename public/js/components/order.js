function Order (props) {

    // STATE DEFINITIONS
    const [order, setOrder] = React.useState(props.order);
    const [viewDiscount, setViewDiscount] = React.useState(false);
    const [validDiscount, setValidDiscount] = React.useState(false);
    const [finishedOrder, setFinish] = React.useState(false);
    const final = [];

    // RENDER BLOCKS
    let discountBlock;
    let appliedDiscount;
    let orderBlock;

    // REACT LIFECYCLE
    React.useEffect(() => {
        setOrder(props.order);
    });

    // ACTION METHODS
    const remove = (childData) => {
        props.onRemove(childData);
    }
    
    const addCode = (code) => {
        let newOrder = order;
        newOrder.discount_applied = code;
        setOrder(newOrder);
        setValidDiscount(true);
    }

    const toggleDiscount = () => {
        setViewDiscount(!viewDiscount);
    }

    const finishOrder = () => {
        let body = order;
        body.status = 'pending';
        body.products = body.products.map(el => el.id);
        axios.post('/api/v1/order', body)
        .then((response) => {
            body.number = response.data.number;
            body.products = order.products;
            setOrder(body);
            setFinish(true);
        })
        .catch(function (error) {
            console.log(error);
        });
    }

    // RENDER CONTROLS 
    for (let  product of order.products) {
        final.push(<li key={product.id}> {product.name} <br/> <button className="button button-clear" onClick={() => remove(product)}> Remove</button> </li>);
    }

    if(viewDiscount === true) {
        discountBlock = <Discount onAddCode={addCode}></Discount>;
    }
    
    if (validDiscount === false) {
        appliedDiscount =  <React.Fragment>
        <div className="order-discount-placeholder">
            {discountBlock}
            <button className="button button-clear btn-block" onClick={() => toggleDiscount()}> 
                {viewDiscount === true? 'Close': 'I Have a discount code'}
            </button>
        </div>
        </React.Fragment>
    } else {
        appliedDiscount = <p> <strong>Discount applied</strong> </p>
    }

    if (finishedOrder === true) {
        orderBlock=<React.Fragment>
            <h4>Order made successfully</h4>
            <p>
                your order number is <strong> {order.number}</strong> 
            </p>
        </React.Fragment>;
    } else {
        orderBlock=<React.Fragment>
            <p> Status: <strong> {order.status}</strong></p>
            <p> Client identificator: {order.user} </p>
            <p>Products:</p>
            <ul className="order-products"> {final} </ul>
            {appliedDiscount}
            <hr/>
            <button className = "button btn-block" onClick={ () => finishOrder()} disabled={order.products.length === 0} > Finish order </button>
        </React.Fragment>;
    }


    return (

        <React.Fragment>
            <section className="row order">
                <div className="column column-100">
                    {orderBlock}
                </div>
            </section>
        </React.Fragment>
    );
}