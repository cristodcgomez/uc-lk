function Product (props) {
    const [product, setProduct] = React.useState(props.data);

    const addToOrder = () => {
        let updatedProduct = product;
        product.selected = true;
        setProduct(updatedProduct);
        props.onSelect({
            id: product.id,
            name: product.name
        });
    }

    return (
        <React.Fragment>
            <article className="card">
                <section className="card-body">
                    <div className="row">
                        <div className="column column-30">
                            <img src="https://via.placeholder.com/300x200"></img>
                        </div>
                        <div className="column column-70">
                            <small> {product.selected === true? 'Selected' : ''}</small>
                            <h3>
                                {product.name}
                            </h3>
                            <p>
                                {product.description? product.description : 'No description available'}
                            </p>
                            <a className="button" onClick={() => addToOrder()}>Add to order</a>
                        </div>
                    </div>
                </section>
            </article>
        </React.Fragment>
    );
}