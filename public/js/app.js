
const
    Link = ReactRouterDOM.Link,
    Route = ReactRouterDOM.Route,
    Redirect = ReactRouterDOM.Redirect,
    Router = ReactRouterDOM.Router;

class App extends React.Component {
    constructor(props) {
        super(props);
    }
    
    checkUser() {
        let session = window.sessionStorage['user'];
        if(!session) {
            return (<Redirect to='/login' />);
        } else {
            let user = JSON.parse(session).data;
            if(user.type === 'admin') {
                return  (<Redirect to='/admin' /> );
            } else {
                return  (<Redirect to='/home' /> );
            }
        }
    }

    render() {
        return (
            <div className="container">
                {this.checkUser()}
                <Route path="/login" component={Login} />
                <Route path="/home" component={Home} />
                <Route path="/admin" component={Admin} />
            </div>
        );
    }
}
