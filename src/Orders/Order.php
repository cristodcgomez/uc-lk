<?php
namespace App\Orders;

use App\Discounts\Discount;
use Conf\BD as BBDD;

class Order {

    private $_id = null;
    public $number = null;
    public $date = null;
    public $user = null;
    public $status = null;
    public $products = null;
    public $discount_applied = null;

    public function __construct($opt = []) {
        $this->_id = isset($opt['id']) ? $opt['id'] : null;
        $this->number = isset($opt['number']) ? $opt['number'] : null;
        $this->date = isset($opt['date']) ? $opt['date'] : NULL;
        $this->user = isset($opt['user']) ? $opt['user'] : NULL;
        $this->status = isset($opt['status']) ? $opt['status'] : NULL;
        $this->products = isset($opt['products']) ? $opt['products'] : NULL;
        $this->discount_applied = isset($opt['discount_applied']) ? $opt['discount_applied'] : NULL;
    }

    public function fetch() {
        $db = new BBDD();
        $res = $db->querySingle("SELECT * FROM orders where id =  '$this->_id'", true);
        if ($res === false) {
            return $this;
        } else {
            $this->id = $res['id'];
            $this->number = $res['number'];
            $this->date = $res['date'];
            $this->user = $res['user'];
            $this->status = $res['status'];
            $this->products = $res['products'];
            $this->discount_applied = $res['discount_applied'];
            return $this;
        }
        $this->_db->close();
    }

    public function upsert($args = null) {
        $db = new BBDD();
        if ($this->hasId()) {
            $this->fetch();
        }
        if ($args) {
            $this->status = isset($args['status']) ? $args['status'] : $this->name;
            $updateElements = "status = '$this->status";

            $res = $db->exec("UPDATE orders SET $updateElements where id =  '$this->_id'");
            if (!$res) {
                echo $db->lastErrorMsg();
                return false;
            } else {
                return $this;
            }
        } else {
            $this->number = 100000 + ($this->user * 100) + rand(1,100); 
            $elements = "(null,'$this->number','$this->date','$this->user','$this->status', '$this->discount_applied')";
            $res = $db->exec("INSERT INTO orders VALUES $elements");
            if (!$res) {
                echo $db->lastErrorMsg();
                return false;
            } else {
                $this->_id = $db->lastInsertRowID();
                $this->id = $this->_id;
                $discount = new Discount(['id' => $this->discount_applied]);
                $discount->upsert(['used' => 1]);
                $end = true;
                foreach ($this->products as $value) {
                    $resProducts = $db->exec("INSERT INTO order_products VALUES ('$value','$this->_id')");
                    if (!$resProducts) {
                        echo $db->lastErrorMsg();
                        $end = false;
                    }
                }
                if($end === true) {
                    return $this;
                } else {
                    return false;
                }
            }
        }
    }

    public function hasId() {
        return $this->_id !== null;
    }
}

