<?php
namespace App\Orders;

class OrderController {

    public function getOne($id)
    {
        $product = new Order(['id' => $id]);
        return $product->fetch();
    }

    public function getMany()
    {
        $list = new OrderList();
        return $list->getAll();
    }

    public function create($args)
    {
        $product = new Order($args);
        return $product->upsert();
    }

    public function edit($id, $args)
    {
        $product = new Order(['id' => $id]);
        return $product->upsert($args);
    }
}

?>