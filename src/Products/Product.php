<?php
namespace App\Products;

use Conf\BD as BBDD;

class Product {

    private $_id = null;
    public $name = null;
    public $description = null;
    public $value = null;

    public function __construct($opt = []) {
        $this->_id = isset($opt['id']) ? $opt['id'] : null;
        $this->name = isset($opt['name']) ? $opt['name'] : null;
        $this->description = isset($opt['description']) ? $opt['description'] : NULL;
        $this->value = isset($opt['value']) ? $opt['value'] : NULL;
        $this->id = $this->_id;
    }

    public function fetch() {
        $db = new BBDD();
        $res = $db->querySingle("SELECT * FROM products where id =  '$this->_id'",true);
        if ($res === false) {
            return $this;
        } else {
            $this->id = $res['id'];
            $this->name = $res['name'];
            $this->description = $res['description'];
            $this->value = $res['value'];
            return $this;
        }
        $this->_db->close();
    }

    public function upsert($args = null) {
        $db = new BBDD();
        if($this->hasId()) {
            $this->fetch();
        }
        if($args) {
            $this->name = isset($args['name']) ? $args['name'] : $this->name;
            $this->description = isset($args['description']) ? $args['description'] : $this->description;
            $this->value = isset($args['value']) ? $args['value'] : $this->value;
            $updateElements = "name = '$this->name', description = '$this->description',value = '$this->value'";

            $res = $db->exec("UPDATE products SET $updateElements where id =  '$this->_id'");
            if (!$res) {
                echo $db->lastErrorMsg();
                return false;
            } else {
                return $this;
            }
        } else {
            $elements = "(null,'$this->name','$this->description','$this->value')";
            $res = $db->exec("INSERT INTO products VALUES $elements");
            if (!$res) {
                echo $db->lastErrorMsg();
                return false;
            } else {
                $this->_id = $db->lastInsertRowID();
                $this->id = $this->_id;
                return $this;
            }
        }
    }

    public function delete() {
        $db = new BBDD();
        $res = $db->exec("DELETE FROM products where id =  '$this->_id'");
        if (!$res) {
            echo $db->lastErrorMsg();
            return false;
        } else {
            return true;
        }
        $this->_db->close();
    }

    public function hasId()
    {
        return $this->_id !== null;
    }
}
