<?php
namespace App\Products;

class ProductController {

    public function __construct() {

    }


    public function getOne($id) {
        $product = new Product(['id' => $id]);
        return $product->fetch();
    }

    public function getMany() {
        $list = new ProductList();
        return $list->getAll();
    }

    public function create($args) {
        $product = new Product($args);
        return $product->upsert();
    }

    public function edit($id, $args) {
        $product = new Product(['id' => $id]);
        return $product->upsert($args);
    }

    public function delete($id) {
        $product = new Product(['id' => $id]);
        return $product->delete();
    }
}


?>