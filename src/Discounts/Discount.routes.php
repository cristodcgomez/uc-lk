<?php

use App\Discounts\DiscountController as Controller;
use Psr\Http\Message\{ResponseInterface as Response, ServerRequestInterface as Request};

$controller = new Controller();

$group->get('/discount[/{id}]', function (Request $request, Response $response, $args) use ($controller) {
    $query = $request->getQueryParams('code');
    if($query) {
        $data = $controller->findByCode($query['code']);
        $payload = json_encode($data);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json');
    } else {
        if($args['id']) {
            $data = $controller->getOne($args['id']);
            $payload = json_encode($data);
            $response->getBody()->write($payload);
            return $response->withHeader('Content-Type', 'application/json');
        } else {
            $data = $controller->getMany();
            $payload = json_encode($data);
            $response->getBody()->write($payload);
            return $response->withHeader('Content-Type', 'application/json');
        }
    }
});

$group->post('/discount', function (Request $request, Response $response, $args) use ($controller) {
    $body = $request->getparsedBody();
    $createdDiscount = $controller->create($body);
    if ($createdDiscount->hasId()) {
        $payload = json_encode($createdDiscount);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(201);
    } else {
        return $response->withStatus(401);
    }
});

$group->put('/discount/{id}', function (Request $request, Response $response, $args) use ($controller) {
    $body = $request->getparsedBody();
    $updatedDiscount = $controller->edit($args['id'], $body);
    if ($updatedDiscount->hasId()) {
        $payload = json_encode($updatedDiscount);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json')->withStatus(202);
    } else {
        return $response->withStatus(401);
    }
});

$group->delete('/discount/{id}', function (Request $request, Response $response, $args) use ($controller) {
    $userData = $controller->delete($args['id']);
    if ($userData === true) {
        $payload = json_encode($userData);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', 'application/json');
    } else {
        return $response->withStatus(401);
    }
});
